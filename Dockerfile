FROM  openjdk:8-jre-slim-buster
ADD target/openapi-api-server-*.jar /zc/
ENTRYPOINT java -jar /zc/openapi-api-server-*.jar
EXPOSE 8080